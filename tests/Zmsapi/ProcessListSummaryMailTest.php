<?php

namespace BO\Zmsapi\Tests;

use Slim\Http\StatusCode;

class ProcessListSummaryMailTest extends Base
{
    protected $classname = "ProcessListSummaryMail";

    public function testRendering()
    {
        $response = $this->render([], ['mail' => 'zms@service.berlinonline.de', 'limit' => 3], []);
        self::assertStringContainsString('Sie haben folgende Termine geplant', (string)$response->getBody());
        self::assertStringContainsString('10118', (string)$response->getBody());
        self::assertStringContainsString('10114', (string)$response->getBody());
        self::assertStringContainsString('10030', (string)$response->getBody());

        self::assertStringContainsString('am Dienstag, 19. April 2016 um 17:40 Uhr', (string)$response->getBody());
        self::assertStringContainsString('am Dienstag, 26. April 2016 um 14:20 Uhr', (string)$response->getBody());
        self::assertStringContainsString('am Montag, 16. Mai 2016 um 08:10 Uhr', (string)$response->getBody());

        self::assertSame(StatusCode::HTTP_OK, $response->getStatusCode());

        //$this->testShortRepetitionFailure();
    }

    private function testShortRepetitionFailure()
    {
        $response = $this->render([], ['mail' => 'zms@service.berlinonline.de', 'limit' => 3], []);

        self::assertSame(StatusCode::HTTP_TOO_MANY_REQUESTS, $response->getStatusCode());
    }

    public function testProcessListEmpty()
    {
        $response = $this->render([], ['mail' => 'not.existing@service.berlinonline.de'], []);
        self::assertStringContainsString('Es wurden keine geplanten Termine gefunden.', (string)$response->getBody());
    }

    public function testUnvalidMail()
    {
        $this->expectException('BO\Mellon\Failure\Exception');
        $this->expectExceptionMessage(
            "Validation failed: no valid email\nno valid DNS entry found\n({mail}=='test@unit')"
        );
        $this->render([], ['mail' => 'test@unit'], []);
    }

    
}
